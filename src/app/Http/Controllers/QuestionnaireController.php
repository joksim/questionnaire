<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function create()
  {
    return view('questionnaire.create');
  }

  public function store()
  {
    # Get the submitted data and validate it.
    $data = request()->validate([
      'title' => 'required',
      'purpose' => 'required'
    ]);

    // Create questionnaire using the relationship to the logged in user
    $questionnaire = auth()->user()->questionnaires()->create($data);

    # Redirect to show the created questionnaire
    return redirect('/questionnaires/' . $questionnaire->id);
  }

  public function show(\App\Questionnaire $questionnaire)
  {
    $questionnaire->load("questions.answers.responses");

    return view('questionnaire.show', compact('questionnaire'));
  }
}
