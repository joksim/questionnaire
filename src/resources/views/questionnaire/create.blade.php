{{-- File: resources/views/questionnaire/show.blade.php --}}

@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Create New Questionnaire</div>
          <div class="card-body">
            <form action="/questionnaires" method="post">
              @csrf

              <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title"
                       class="form-control" placeholder="Enter Title"
                       aria-describedby="titleHelp"/>
                <small id="titleHelp" class="form-text text-muted">Give your questionnaire a title</small>

                {{-- Display errors from verification--}}
                @error('title')
                <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group">
                <label for="purpose">Purpose</label>
                <input type="text" name="purpose" id="purpose"
                       class="form-control" placeholder="Enter purpose"
                       aria-describedby="purposeHelp"/>
                <small id="purposeHelp" class="form-text text-muted">Giving a purpose will increase responses</small>

                {{-- Display errors from verification--}}
                @error('purpose')
                <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>

              <button type="submit" class="btn btn-primary">Create Questionnaire</button>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
